package pl.sda;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj wagę w kilogramach i wzrost w centymetrach");
        float weight = sc.nextFloat();
        int height = sc.nextInt();
        double bmi = weight / Math.pow(height * 0.01, 2);
        System.out.println("BMI = " + bmi);

        if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("BMI optymalne");
        } else {
            System.out.println("BMI nieoptymalne");
        }


    }
}
